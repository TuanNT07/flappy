﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public bool isEndGame;
    bool isStartFirstTime = true; // Bien kiem tra xem day co phai la dau tien bat dau game hay khong 
    int gamePoint = 0; //Bien diem so 
    public Text TxtPoint; // Lay Text (Point) ben ngoai vao (Phai ra Unity keo Point(Text) vao thuoc tinh Txt Point cua component Script Game Controller cua GameObject GameController
    public GameObject pnlEndGame; // GameObject pnl EndGame (no se tro thanh 1 thuoc tinh cua component Script Game Controller cua GameObject GameController : se phai keo tha cai GameObject EndGamePanel(GObj con cua Canvas) vao cai khung cua thuoc tinh nay) (!)
    public Text txtEndPoint; //Text trong Panel EndGame (Lam tuong tu (!))
    public Button btnRestart; //Button Restart trong Panel EndGame(Lam tuong tu(!))

	// Use this for initialization
	void Start () {
        Time.timeScale = 0; // Moi dau khi vao game thi man hinh dung im
        isEndGame = false; // Moi vao game thi isEndGame = false
        pnlEndGame.SetActive(false);// Luc moi vao game thi chua kich hoat pnlEndGame
        isStartFirstTime = true; // Moi vao game (Lan dau tien bat dau game)
    }
	
	// Update is called once per frame
	void Update () {
        if(isEndGame) // Neu da ket thuc game roi (va khi nguoi dung an chuot trai thi load game lai)
        {
            if (Input.GetMouseButtonDown(0) && isStartFirstTime) // Neu nguoi dung an chuot trai (va day la lan dau tien chay game)
            {
                StartGame(); // Goi ham StartGame
            }
        }
	    else // Khong phai EndGame nhung nguoi dung van nhan phim 
        {
            if (Input.GetMouseButtonDown(0)) // Neu nguoi dung an chuot trai 
            {
                Time.timeScale = 1; // Tra thoi gian ve lai binh thuong game bat dau chay 
            }
        }
	}

    public void GetPoint() // Ham tinh diem
    {
        gamePoint++; // Diem tang 1 don vi 
        TxtPoint.text = "Point: " + gamePoint.ToString();
    }

    void StartGame() // Ham bat dau game
    {
        SceneManager.LoadScene(0); // Load man choi
    }

    public void Restart() // Ham Restart game
    {
        StartGame();
    }
    public void EndGame() // Ham ket thuc game 
    {
        isEndGame = true; // Da ket thuc game
        isStartFirstTime = false; // Ket thuc game khong phai lan dau tien chay game
        Time.timeScale = 0; // Dong bang man hinh cua game luc EndGame.
        pnlEndGame.SetActive(true); //Kich hoat pnlEndGame (cho pnlEndGame hien thi khi EndGame)
        txtEndPoint.text = "Your point \n" + gamePoint.ToString(); //Hien thi diem
    }
}
