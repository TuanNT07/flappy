﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {

    public float flyPower = 50; // Luc nhan tac dong len chim, khi nhan chuot trai

    public AudioClip flyClip;    // Am thanh luc nguoi dung nhan chuot trai (Ra ngoài Unity keo âm thanh tương ứng vào thuộc tính Fly Clip)
    public AudioClip gameOverClip; // Am thanh luc GameOver (Ra ngoài Unity kéo âm thanh tương ứng vào thuộc tính Game Over Clip)

    private AudioSource audioSource;// Khai bao AudioSource dat ten la audioSource
    private Animator anim; // Lay Animation ra 

    GameObject obj; // Lay game Object ra
    public GameObject gameController; // Lay ra mot cai GameObject gameController ben Script BirdController
	// Use this for initialization
	void Start () {
        obj = gameObject;
        audioSource = obj.GetComponent<AudioSource>();
        audioSource.clip = flyClip;  // Set Audio mac dinh la flyClip
        anim = obj.GetComponent<Animator>();
        anim.SetFloat("flyPower",0); //Set gia tri mac dinh cho parameter flyPower = 0
        anim.SetBool("isDead",false);//Set gia tri mac dinh cho parameter isDead = false

        if (gameController == null) // gia su quen keo GameController
        {
            gameController = GameObject.FindGameObjectWithTag("GameController"); // Tim 1 doi tuong co Tag GameController 
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetMouseButton(0)) // Gan phim chuot trai
        {
            if(!gameController.GetComponent<GameController>().isEndGame) // Khi game chua ket thuc thi moi duoc play nhac
                audioSource.Play(); //Lay cai audioSource ra de Play no
            obj.GetComponent<Rigidbody2D> ().AddForce(new Vector2(0, flyPower)); // add luc day khi bam chuot trai cho Bird           
        }
        anim.SetFloat("flyPower",obj.GetComponent<Rigidbody2D>().velocity.y); // velocity : luc 
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        EndGame();
    }

    void OnTriggerEnter2D(Collider2D other) // Khi con Bird bay qua khoang trong giua 2 cay cot
    {
        gameController.GetComponent<GameController>().GetPoint(); //Goi ham GetPoint (Ham tinh diem) tu ben Script GameController
    }
    void EndGame()
    {
        anim.SetBool("isDead",true);//Set gia tri cho parameter isDead = true khi EndGame
        audioSource.clip = gameOverClip;
        audioSource.Play();
        gameController.GetComponent<GameController>().EndGame(); // Goi Ham EndGame() tu Script GameController
    }
}
