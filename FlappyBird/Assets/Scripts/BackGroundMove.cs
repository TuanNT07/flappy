﻿using UnityEngine;
using System.Collections;

public class BackGroundMove : MonoBehaviour {

    // toc do di chuyen cua Object, sau khi khai bao bien se tu dong add them 1 thuoc tinh Move Speed vao trong component Script Back Ground Move
    public float moveSpeed;
    //Pham vi di chuyen cua Object, toi khi Object di chuyen vuot qua pham vi do thi reset ve vi tri ban dau
    public float moveRange;

    private Vector3 oldPosition;  // Vi tri ban dau cua Object
    private GameObject obj;       // bieu thi cho Object ma script duoc gan vao
	// Use this for initialization
	void Start () {
        obj = gameObject;
        oldPosition = obj.transform.position;
        moveSpeed = 5;
        moveRange = 20;
	}
	
	// Update is called once per frame
	void Update () {
        // Tu toa do cua transform nay, tuc la toa do cua object dang gan scrip nay, no se dich chuyen them 1 cai vector nhu the nay(cong them vector do vao trong position cua Object)
        obj.transform.Translate(new Vector3(-1 * Time.deltaTime * moveSpeed, transform.position.y, 0));

        // khoang cach tu toa do hien tai cua Object den vi tri ban dau > Pham vi dich chuyen cua Object
        if(Vector3.Distance(oldPosition, obj.transform.position) > moveRange)
        {
            obj.transform.position = oldPosition; // reset Object ve vi tri ban dau
        }
    }
}
